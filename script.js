const stepNumEl = document.querySelectorAll('.circle');
const pagesEl = document.querySelectorAll('.form');
const page1El = document.getElementById('page__1');
const formEl = document.querySelector('.requires-validation');
const planCardsEl = document.querySelectorAll('.plan__card');
const addonsCardsEl = document.querySelectorAll('.addon__card');
const changePlanEl = document.getElementById('change__plan');
const thankYouPage = document.getElementById('thank-you');
const formInputEl = page1El.querySelectorAll('.form-control');

// Initially thankyou page will be hidden
thankYouPage.setAttribute('hidden', 'hidden');

// empty object to store selected plan, price and duration
let selectedPlan = {};

// function for storing selected add-on plan
function selectedAddons() {
  const addOnArr = [];
  addonsCardsEl.forEach((card) => {
    let planName = card.querySelector('.card__name').innerText;
    let planPrice = card.querySelector('.subscription__price').innerText;
    let planDuration = card.querySelector('.subscription__duration').innerText;

    if (card.classList.contains('selected')) {
      addOnArr.push({
        planName,
        planPrice,
        planDuration,
      });
    }
  });
  return addOnArr;
}

// plan prices
const monthlyPlanPrices = [9, 12, 15];
const yearlyPlanPrices = [90, 120, 150];
const monthlyAddonPrices = [1, 2, 2];
const yearlyAddonPrices = [10, 20, 20];

// SetPlan function
function setPlan(cards, price, duration) {
  Array.from(cards).forEach((card, idx) => {
    card.querySelector('.subscription__price').innerText = `${price[idx]}`;
    card.querySelector('.subscription__duration').innerText = `${duration}`;
  });
}

// set default price and duration of the cards
setPlan(planCardsEl, monthlyPlanPrices, 'mo');
setPlan(addonsCardsEl, monthlyAddonPrices, 'mo');

// Buttons
const nextBtn = document.getElementById('next-button');
const prevBtn = document.getElementById('prev-button');

// step number
let stepNum = 0;

const selectPlanError = (text) => {
  document.getElementById('select-plan-error').innerText = text;
};

nextBtn.addEventListener('click', (e) => {
  e.preventDefault();
  if (stepNum === 0) {
    if (!formValidation()) return;
    stepNum++;
    showStep(stepNum);
  } else if (stepNum === 1) {
    if (Object.entries(selectedPlan).length === 0) {
      return selectPlanError('Please select a plan');
    }
    stepNum++;
    showStep(stepNum);
  } else if (stepNum === 2) {
    showTotal();
    stepNum++;
    showStep(stepNum);
  } else if (stepNum === 3) {
    stepNum++;
    showStep(stepNum);
    thankYouPage.removeAttribute('hidden');
  } else return;
});

prevBtn.addEventListener('click', (e) => {
  e.preventDefault();
  if (stepNum > 0) {
    stepNum--;
  }

  return showStep(stepNum);
});

// Function to handle steps
function showStep(step) {
  selectPlanError('');

  const totalSteps = stepNumEl.length;

  if (step < totalSteps) {
    Array.from(stepNumEl).map((ele) => {
      ele.classList.remove('active');
    });

    stepNumEl[step].classList.add('active');
  }

  Array.from(pagesEl).map((page) => {
    page.classList.remove('active');
  });

  if (step === 3) {
    pagesEl[3].classList.add('active');
  } else {
    pagesEl[step].classList.add('active');
  }

  // handle form step
  if (step < pagesEl.length) {
    if (step === 0) {
      prevBtn.classList.add('hidden');
      prevBtn.setAttribute('disabled', '');
    } else if (step === 4) {
      nextBtn.parentElement.parentElement.classList.add('hidden');
    } else {
      prevBtn.classList.remove('hidden');
      prevBtn.removeAttribute('disabled');
    }
    if (step === 3) {
      nextBtn.innerText = 'confirm';
      nextBtn.classList.add('confirm-btn');
      nextBtn.classList.remove('btn-primary');
    } else {
      nextBtn.innerText = 'Next Step';
    }
  }
}

showStep(stepNum);

// STEP-1 | PERSONAL INFO [ FORM-VALIDATION ]

// function to add warning
function showError(input, warningText) {
  input.classList.add('error');
  input.parentElement.querySelector('.warning').innerText = warningText;
}

// function to remove warning
function hideError(input) {
  input.classList.remove('error');
  input.parentElement.querySelector('.warning').innerText = '';
}

function formValidation() {
  let isValid = true;

  formInputEl.forEach((input) => {
    // username
    let nameRegExp = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
    if (input.name === 'username') {
      if (input.value.length === 0) {
        showError(input, 'This field is required');
        isValid = false;
      } else if (!input.value.match(nameRegExp)) {
        showError(input, 'Enter a valid name');
        isValid = false;
      } else {
        hideError(input);
      }
    }

    // email
    if (input.name === 'email') {
      const emailRegExp = /^[^ ]+@[^ ]+.[a-z]{2,3}$/;
      if (input.value.length === 0) {
        showError(input, 'This field is required');
        isValid = false;
      } else if (!input.value.match(emailRegExp)) {
        showError(input, 'Enter valid email');
        isValid = false;
        failure;
      } else {
        hideError(input);
      }
    }
    // phone number
    if (input.name === 'phone') {
      let phoneRegExp = /^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$/;
      if (input.value.length === 0) {
        showError(input, 'This field is required');
        isValid = false;
      } else if (!input.value.match(phoneRegExp)) {
        showError(input, 'Enter a valid number');
        isValid = false;
      } else {
        hideError(input);
      }
    }
  });

  if (!formEl.checkValidity()) {
    isValid = false;
  }

  return isValid;
}

// STEP-2 | SELECT PLAN & TOGGLE BUTTON
const toggleEl = document.getElementById('toggle');
const yearlyBenefitEl = document.querySelectorAll('.yearly__benefit');
const monthEl = document.getElementById('monthly');
const yearEl = document.getElementById('yearly');

toggle.addEventListener('click', (e) => {
  selectPlanError('');

  const toggle = e.currentTarget.parentElement;

  planCardsEl.forEach((card) => card.classList.remove('selected'));
  selectedPlan = {};

  toggle.classList.toggle('active');

  // If toggle active [yearly] else [monthly]
  if (toggle.classList.contains('active')) {
    yearlyBenefitEl.forEach((item) => item.classList.add('show'));
    setPlan(planCardsEl, yearlyPlanPrices, 'yr');
    setPlan(addonsCardsEl, yearlyAddonPrices, 'yr');
    yearEl.classList.add('selected__plan');
    monthEl.classList.remove('selected__plan');
  } else {
    setPlan(planCardsEl, monthlyPlanPrices, 'mo');
    setPlan(addonsCardsEl, monthlyAddonPrices, 'mo');
    yearlyBenefitEl.forEach((ele) => ele.classList.remove('show'));
    monthEl.classList.add('selected__plan');
    yearEl.classList.remove('selected__plan');
  }
});

// Select plan card
planCardsEl.forEach((card) => {
  card.addEventListener('click', (e) => {
    selectPlanError('');

    let targetEl = e.currentTarget;
    planCardsEl.forEach((card) => card.classList.remove('selected'));

    targetEl.classList.add('selected');

    let planName = targetEl.querySelector('.card-title').innerText;
    let planPrice = targetEl.querySelector('.subscription__price').innerText;
    let planDur = targetEl.querySelector('.subscription__duration').innerText;

    return (selectedPlan = { planName, planPrice, planDur });
  });
});

// STEP3 | ADD-ON

addonsCardsEl.forEach((card) => {
  card.addEventListener('click', (e) => {
    let target = e.currentTarget;

    let checkbox = target.querySelector('.checkbox');
    target.classList.toggle('selected');

    if (target.classList.contains('selected')) {
      return (checkbox.checked = true);
    } else {
      return (checkbox.checked = false);
    }
  });
});

// STEP-4

// SHOW SELECTED PLAN, SELECTED ADD-ON, AND TOTAL AMOUNT
function showTotal() {
  let totalAmount = 0;

  const planDuration = selectedPlan.planDur === 'mo' ? 'Monthly' : 'Yearly';

  const planEl = document.getElementById('selected__plan');
  const addonListEl = document.getElementById('selected-addon');
  const totalEl = document.getElementById('total');

  planEl.innerHTML = '';
  addonListEl.innerHTML = '';
  totalEl.innerHTML = '';

  let planName = document.createElement('p');
  planName.innerText = selectedPlan.planName;

  let duration = document.createElement('p');
  duration.innerText = `(${planDuration})`;

  let planPrice = document.createElement('p');
  planPrice.innerText = `$${selectedPlan.planPrice}/${selectedPlan.planDur}`;

  planEl.appendChild(planName);
  planEl.appendChild(duration);
  planEl.appendChild(planPrice);

  totalAmount += Number(selectedPlan.planPrice);

  selectedAddons().forEach((addon) => {
    let listItem = document.createElement('li');

    let addonName = document.createElement('p');
    addonName.innerText = addon.planName;

    let addonPrice = document.createElement('p');
    addonPrice.innerText = `+$${addon.planPrice}/${addon.planDuration}`;

    listItem.appendChild(addonName);
    listItem.appendChild(addonPrice);

    addonListEl.appendChild(listItem);

    totalAmount += Number(addon.planPrice);
  });

  // Display the total amount
  totalEl.innerHTML = `<span>Total(
    per ${planDuration.slice(0, -2).toLocaleLowerCase()}) </span> 
      <span> $${totalAmount}/${selectedPlan.planDur}</span>`;
}

// function to handle change button
changePlanEl.addEventListener('click', () => {
  stepNum = 0;
  showStep(stepNum);
});

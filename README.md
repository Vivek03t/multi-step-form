## Overview

### The challenge

Users should be able to:

- Complete each step of the sequence
- Go back to a previous step to update their selections
- See a summary of their selections on the final step and confirm their order
- View the optimal layout for the interface depending on their device's screen size
- See hover and focus states for all interactive elements on the page
- Receive form validation messages if:
  - A field has been missed
  - The email address is not formatted correctly
  - A step is submitted, but no selection has been made

### Screenshot
<!-- 
![](./screenshots/page-1.png)
![](./screenshots/page-2.png)
![](./screenshots/page-3.png)
![](./screenshots/page-4.png) -->

### Links

- Solution URL: [https://gitlab.com/Vivek03t/multi-step-form](https://gitlab.com/Vivek03t/multi-step-form)
- Live Site URL: [https://multi-step-form-chi-three.vercel.app/](https://multi-step-form-chi-three.vercel.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- JavaScript
- [Bootstrap](https://getbootstrap.com/docs/5.3/forms/overview/) - CSS Framework


### What I learned


### Useful resources


## Author
- GitHub - [@vivek](https://github.com/tivivek)


## Acknowledgments